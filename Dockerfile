FROM openjdk:8-jre
ADD seckill.jar seckill.jar
ADD application.yml application.yml
ENTRYPOINT ["java","-jar","seckill.jar"]