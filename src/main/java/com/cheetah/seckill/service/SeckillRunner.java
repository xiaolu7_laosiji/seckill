package com.cheetah.seckill.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cheetah.seckill.mapper.SeckillMapper;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class SeckillRunner implements CommandLineRunner {

    @Autowired
    private SeckillMapper seckillMapper;
    @Autowired
    private RedissonClient redisClient;
    @Override
    public void run(String... args) throws Exception {
        this.seckillMapper.selectList(new QueryWrapper<>()).forEach(item -> {
            RAtomicLong bucket = redisClient.getAtomicLong("sec_" + item.getId());
            bucket.set(item.getNumber());
        });
    }
}
