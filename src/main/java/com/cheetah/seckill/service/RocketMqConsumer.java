package com.cheetah.seckill.service;

import com.cheetah.seckill.entity.Order;
import com.cheetah.seckill.entity.Seckill;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RocketMQMessageListener(topic = "topic_order", consumerGroup = "seckill_consumer")
public class RocketMqConsumer implements RocketMQListener<Order> {
    @Autowired
    private SeckillService seckillService;

    @Override
    public void onMessage(Order message) {
        this.seckillService.startSeckill(message);
    }
}
