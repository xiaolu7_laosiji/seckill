package com.cheetah.seckill.service;

import com.cheetah.seckill.entity.Order;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RocketMQMessageListener(topic = "order_cancel", consumerGroup = "order_cancel_consumer")
public class OrderCancelConsumer implements RocketMQListener<Order> {
    @Autowired
    private SeckillService seckillService;
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Override
    public void onMessage(Order message) {
        logger.info("自动取消:" + message.getId());
        this.seckillService.cancel(message);
    }
}
