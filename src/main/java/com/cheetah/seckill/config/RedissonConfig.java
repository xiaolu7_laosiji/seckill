package com.cheetah.seckill.config;

import org.apache.commons.lang3.StringUtils;
import org.redisson.Redisson;
import org.redisson.api.RQueue;
import org.redisson.api.RedissonClient;
import org.redisson.config.ClusterServersConfig;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedissonConfig {
    @Value("${org.redisson.nodes}")
    private String nodes;

    @Bean
    RedissonClient client() {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://" + nodes);
        return Redisson.create(config);
    }
}
