package com.cheetah.seckill.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
    @RequestMapping("/{url}.html")
    public String module(@PathVariable("url") String url){
        return url;
    }
}
