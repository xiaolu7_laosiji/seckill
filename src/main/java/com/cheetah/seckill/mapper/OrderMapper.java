package com.cheetah.seckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cheetah.seckill.entity.Order;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderMapper extends BaseMapper<Order> {
}
