package com.cheetah.seckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cheetah.seckill.entity.Seckill;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface SeckillMapper extends BaseMapper<Seckill> {
    @Update("update seckill set number=number-1 WHERE id=#{id}")
    public int updateNum(@Param("id") long id);
    @Update("update seckill set number=number+1 WHERE id=#{id}")
    public int cancel(@Param("id") long id);
}
