# 预备工作

**安装docker**

参考下面的文件就可以，很详细

https://yq.aliyun.com/articles/110806?spm=5176.8351553.0.0.58861991FsirjY

**配置docker**

使用国内镜像加速：

```
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://tyoqtgc3.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```

安装docker-compose, 在线安装太慢了，我用的是以前下载的一个离线包。

链接: https://pan.baidu.com/s/1J3LW27RdJ62mOv2BQNj8zA 提取码: 8hrx

# 秒杀系统之docker构建   

首先，我们在虚拟机上，创建一个目录，**mkdir /opt/soft/seckill**, 之后**vi Dockerfile**

```
FROM openjdk:8-jre
ADD seckill.jar seckill.jar
ADD application.yml application.yml
ENTRYPOINT ["java","-jar","seckill.jar"]
```

第二步，**vi docker-compose.yml**

```yaml
version: '3.0'
services:
 redis:
  image: redis:5.0.9
  command: redis-server --appendonly yes --protected-mode no
  volumes:
   - /opt/soft/seckill/data:/data
  ports:
   - 6379:6379
  deploy:
   replicas: 2
 app:
  build:
   context: .
   dockerfile: Dockerfile
  container_name: app
  image: spring-app:1.0
  ports:
   - "8080:8080"
```

# swarm集群

之后，我们可以将swarm集群安装上，在192.168.3.24虚拟机上，执行以下命令：

**docker swarm init --advertise-addr 172.16.86.47**

之后，在192.168.3.7上，执行以下命令：

**docker swarm join --token SWMTKN-1-2138ygwxg7jamh0e31y6u41gyombr94g2nftvexqe5jmgkoa1v-9dmuza6u4i3gywl07qwbsu7x6 192.168.3.24:2377**

在192.168.3.24上，执行**docker node ls**验证

```
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
0m2i2v5p8kupa38mmv0si62pq *   centos1             Ready               Active              Leader              19.03.5
d5fc27l3gxq6n7qqfn8yq9373     centos2             Ready               Active                                  19.03.5
```

# 启动项目

在192.168.3.24和192.168.3.7上，分别执行 **mkdir -p /opt/soft/seckill/data**

之后执行以下命令：

```shell
cd /opt/soft/seckill
# 注意一定要重新构建，避免使用到了缓存
docker-compose build --force-rm --no-cache  
docker stack deploy -c docker-compose.yml web
```

之后，执行**docker service ls**验证

```
ID                  NAME                MODE                REPLICAS            IMAGE               PORTS
ct1gmbxptp8g        web_app             replicated          1/1                 spring-app:1.0      *:8080->8080/tcp
k8175powp2h9        web_redis           replicated          2/2                 redis:5.0.9         *:6379->6379/tcp
```

也可以使用浏览器进行访问：http://192.168.3.24:8080/seckill/list